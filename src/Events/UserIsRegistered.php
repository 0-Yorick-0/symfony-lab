<?php
/**
 * Roxed by :
 * User: yferlin
 * Date: 23/01/2020
 * No shit !
 */

namespace App\Events;

use Symfony\Contracts\EventDispatcher\Event;

class UserIsRegistered extends Event
{
    public const NAME = 'user.is.registered';

    protected $userName;
    protected $userMail;

    public function __construct(
        string $userName,
        string $userMail
    )
    {
        $this->userName = $userName;
        $this->userMail = $userMail;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @return string
     */
    public function getUserMail()
    {
        return $this->userMail;
    }
}
<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
         $user = new User();
        $user->setUsername('David')
            ->setPassword($this->passwordEncoder->encodePassword(
           $user,
           'prometheus'
        ))
            ->setEmail('alieneverywhere@earth.com')
            ->setRoles(['ROLE_SUPER_ADMIN'])
        ;

        $manager->persist($user);
        $manager->flush();
    }
}

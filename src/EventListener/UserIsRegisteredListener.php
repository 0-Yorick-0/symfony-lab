<?php
/**
 * Roxed by :
 * User: yferlin
 * Date: 23/01/2020
 * No shit !
 */

namespace App\EventListener;

use App\Events\UserIsRegistered;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class UserIsRegisteredListener
{
    /**
     * @var MailerInterface
     */
    private $mailer;
    private $noReplyEmail;

    /**
     * UserIsRegisteredListener constructor.
     * @param MailerInterface $mailer
     * @param $noReplyEmail
     */
    public function __construct(
        MailerInterface $mailer,
        string $noReplyEmail
    )
    {
        $this->mailer = $mailer;
        $this->noReplyEmail = $noReplyEmail;
    }

    /**
     * @param UserIsRegistered $event
     */
    public function onUserIsRegistered(UserIsRegistered $event)
    {
        //TemplatedEmail permet l'inclusion de template twig
        //https://symfony.com/doc/4.4/mailer.html#twig-html-css
        $email = (new TemplatedEmail())
            ->from($this->noReplyEmail)
            ->to(new Address($event->getUserMail()))
            ->subject('T\'es inscrit gros !')
            ->htmlTemplate('emails/signup.html.twig')
            ->context([
                'expiration_date' => new \DateTime('+7 days'),
                'username' => $event->getUserName()
            ])
            ;

        $this->mailer->send($email);
    }
}
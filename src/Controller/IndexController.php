<?php
/**
 * Roxed by :
 * User: yferlin
 * Date: 21/01/2020
 * No shit !
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route(
     *     name="home",
     * )
     */
    public function index(Request $request)
    {
        return $this->render('index/index.html.twig', []);
    }
}